<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlacklistUrlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blacklist_urls')->insert([
            'regex' => '^(https?:\/\/([\w\d]+\.)?vnexpress\.net)'
        ]);
    }
}
