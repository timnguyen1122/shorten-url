import moment from "moment";
import ReactDOM from 'react-dom';
import React, {useEffect, useState} from 'react';
import {withRouter} from "react-router-dom";
import { Link } from 'react-router-dom'
import FlashMessage from "react-flash-message";
import ClipLoader from "react-spinners/ClipLoader";
import ShortenedForm from "./ShortenedForm";
import ShortUrlApi from "./apis/ShortUrlApi";

const Main = () => {
    const [loading, setLoading] = useState(false);
    const [shortUrls, setShortUrls] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');
    const [sucessMessage, setSuccessMessage] = useState('');
    const [newShortLink, setNewShortLink] = useState('');
    const [filter, setFilter] = useState('');

    useEffect(() => {
        ShortUrlApi.getRecent().then(({ data: responseData }) => {
            setShortUrls(responseData.data);
        }).catch(err => {
            setErrorMessage(err.message || 'There is an error');
        }).finally(() => {
            setLoading(false);
        });
    }, [])

    const handleAddUrl = async (urlItem) => {
        const { data: responseData, status } = await ShortUrlApi.add(urlItem);
        if (status === 200 && responseData.success) {
            setShortUrls([...[responseData.data], ...shortUrls]);
            setSuccessMessage('Short URL successfully generated!');
            setNewShortLink(responseData.data.short_code)

            if (shortUrls.length > 10) {
                setShortUrls(shortUrls.splice(-1, 1));
            }
        } else {
            setErrorMessage(responseData.error || 'System Issue. Please Contact us');
        }
    }

    const border = (index) => {
        return index = shortUrls.length - 1 ? 'entry-content last' : 'entry-content';
    }

    const urlsList = filter !== '' ? shortUrls.filter(u => u.short_code.startsWith(filter)) : shortUrls;

    return (
        <div className="content pt-5">
            <div className="container">
                {
                    errorMessage ?
                        <FlashMessage duration={3000} persistOnHover={true}>
                            <h5 className={"alert alert-danger"}>{errorMessage}.</h5>
                        </FlashMessage> : ''
                }
                {
                    sucessMessage ?
                        <FlashMessage duration={3000} persistOnHover={true}>
                            <h5 className={"alert alert-success"}>{sucessMessage}.</h5>
                        </FlashMessage> : ''
                }

                <ShortenedForm onAdd={handleAddUrl}  />
                {
                    newShortLink ?
                        <div className="mb-3 mt-2 text-center p-3 bg-info text-white">
                            <h4>Your Short URL is: </h4>
                            <Link className="text-light" to={newShortLink} target="_blank">{window.location.protocol}//{window.location.hostname}/{newShortLink}</Link>
                        </div>: ''
                }

                <div className="sweet-loading mt-5">
                    <ClipLoader
                        size={150}
                        color={"#123abc"}
                        loading={loading}
                    />
                </div>
                <div className="pb-5">
                    <div className="row mb-3">
                        <div className="col-3">
                            <h2 className="mb-1">Recent Links</h2>
                        </div>
                        <div className="col-4">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Find by short code"
                                name="filter"
                                required
                                value={filter}
                                onChange={e => setFilter(e.target.value)}
                            />
                        </div>
                    </div>
                    {
                        urlsList.length > 0 ? (
                            <div>
                            {
                                urlsList.map((link,index) =>
                                    <div  className={border(index)}  >
                                        <ul>
                                            <li key={link.short_code}><Link to={link.short_code} target="_blank">{window.location.protocol}//{window.location.hostname}/{link.short_code}</Link></li>
                                            <time>{moment(link.created_at).fromNow()}</time>
                                            <li key={link.id}><a href={link.long_url} target="_blank" class="text-muted">{link.long_url}</a></li>
                                            {link.expired_at && <li>Expired at: {moment(link.expired_at).toLocaleString()}</li>}
                                            <li>Views: {link.number_visit}</li>
                                        </ul>
                                    </div>
                                )
                            }
                            </div>
                        ) : <div>No Short URL has been generated yet!</div>
                    }
                </div>
            </div>
        </div>
    )
}

export default withRouter(Main);
