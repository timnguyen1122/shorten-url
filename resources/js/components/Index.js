import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route} from 'react-router-dom';
import Main from './Main';

const Index = () => {
    return (
        <div id="app" key="app-url-shortener">
        <BrowserRouter>
            <Route component={Main} />
        </BrowserRouter>
        </div>
    )
}

export default Index;

if (document.getElementById('index')) {
    ReactDOM.render(<Index />, document.getElementById('index'));
}
