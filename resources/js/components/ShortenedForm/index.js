import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import { withRouter } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const ShortenedForm = ({
    onAdd
}) => {
    const [longUrl, setLongUrl] = useState('');
    const [expiredAt, setExpiredAt] = useState('');
    const [isPrivate, setPrivate] = useState(0);

    const handleSubmit = (e) => {
        e.preventDefault();
        const newUrlItem = {
            long_url: longUrl,
            expired_at: expiredAt,
            private: isPrivate
        };
        if (expiredAt === '') {
            delete newUrlItem.expired_at;
        }

        setLongUrl('');
        setExpiredAt('');
        setPrivate(0);

        onAdd(newUrlItem);
    }

    return (
        <div className="row pb-2">
            <div className="col">
                <form onSubmit={handleSubmit}>
                    <fieldset>
                        <div className="row">
                            <div className="col-4">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Long URL (required)"
                                    name="long_url"
                                    required
                                    value={longUrl}
                                    onChange={e => setLongUrl(e.target.value)}
                                />
                            </div>
                            <div className="col-4">
                                <DatePicker
                                    className="form-control full-width"
                                    selected={expiredAt}
                                    onChange={(date) => setExpiredAt(date)}
                                    showTimeSelect
                                    dateFormat="MMMM d, yyyy h:mm aa"
                                    placeholderText="Expired at (optional)"
                                />
                            </div>
                            <div className="col-2">
                                <input
                                    type="checkbox"
                                    className="form-check-input mt-3"
                                    id="is_it_private"
                                    name="private"
                                    checked={isPrivate}
                                    onChange={e =>  setPrivate(e.target.checked)}
                                />
                                <label className="form-check-label mt-2" for="is_it_private">Private?</label>
                            </div>
                            <div className="col-2">
                                <button type="submit" className="btn btn-primary pl-5 pr-5">Shorten</button>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
        </div>

    );
}

export default withRouter(ShortenedForm);
