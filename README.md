# Shorten URL

## Quick way to setup (using docker compose)

- `docker-compose up -d`
## How to setup manually

- `git clone https://timnguyen1122@bitbucket.org/timnguyen1122/shorten-url.git`

### Setup infrastructure
- `git clone https://github.com/Laradock/laradock.git`
- `cd laradock && cp .env.example .env`
- `docker-compose up -d nginx php-fpm postgres`

### Setup PHP dependences & init project settings
- `composer update && composer install`
- `cp .env.exampe .env`
- `php artisan key:generate`
- `php artisan migrate`

### Run seeding data
- `php artisan db:seed`

### Setup ReactJS dependences
- `yarn && yarn prod`

### Run development server
- `php artisan serve`

### Visit website
- http://localhost:8000


## How to test blocked URL?

Let try this URL: https://vnexpress.net/du-kien-1-000-ca-covid-19-xuat-vien-moi-ngay-o-tp-hcm-4328465.html

Most of URL from domain https://vnexpress.net are blocked by Regex `^(https?:\/\/([\w\d]+\.)?vnexpress\.net)`. We can add more blacklist domain in the `blacklist_urls` table.