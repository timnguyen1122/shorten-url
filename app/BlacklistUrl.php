<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlacklistUrl extends Model
{

    protected $fillable = [
        'regex'
    ];
    protected $hidden = ['updated_at'];


}
