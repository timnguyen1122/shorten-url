<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicUrl extends Model
{
    protected $fillable = [
        'long_url', 'short_code', 'number_visit', 'private', 'expired_at'
    ];
    protected $hidden = ['updated_at', 'deleted_at'];
}

